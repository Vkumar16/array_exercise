const filter = require('./filter')

const items = [1, 2, 3, 4, 5, 5];
const filterData = filter(items, function (eachElement) {
    if (eachElement % 2 == 0)
        return eachElement;
})

console.log("After calling filter function filter datas ", filterData);