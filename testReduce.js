const items = [1, 2, 3, 4, 5, 5];
const reduce = require('./reduce')
const reduceData = reduce(items , function(eachData, previousValue) {
    return eachData + previousValue;
})
console.log("After calling reduce function value of reduceData is ",reduceData);