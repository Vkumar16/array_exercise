
function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test

  let filterData = [];
  for (let i = 0; i < elements.length; i++) {
    let data = cb(elements[i])
    if (data)
      filterData.push(data);
  }
  return filterData;
}

module.exports = filter
