function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
  let newArray = []
  for (let i = 0; i < elements.length; i++) {

    if (Array.isArray(elements[i])) {

      newArray = newArray.concat(flatten(elements[i]));
      //console.log(newArray);


    }
    else {
      // console.log(elements[i]);
      newArray.push(elements[i]);
    }
  }
  return newArray;
}
module.exports = flatten 
