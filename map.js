
function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, 
    //and functionructs a new       array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    // addind each element of elements with 10;
    let newArray = []
    for (let i = 0; i < elements.length; i++) {
        let data = cb(elements[i],i,elements);
        //console.log(data)
        newArray.push(data);
    }
    return newArray;
}
module.exports = map
