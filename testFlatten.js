const flatten = require('./flatten')
const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
const newArray = flatten(nestedArray);
console.log(newArray);