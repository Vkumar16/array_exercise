const map = require('./map')
const items = [1, 2, 3, 4, 5, 5];
console.log("Before Calling map function Items datas are : ", items);
// for first line for feedback
const fun = (eachEleent, index, array) => {
    console.log(index, eachEleent, array);
    return eachEleent + 10;
}
//for feedback's third line  

// const fun1 = (element,index) =>{
//     if(index < 3)
//         return element;

// }
// const data1 =  map([1,2,3,4],fun1)
// console.log(data1)

const data = map(items, fun)
console.log("After Calling map function mapdata datas are : ", data);

// for feedback's second line 

// ["1", "2", "3"].map(parseInt) //output [1,NaN,NaN]
// to solve this problem we have to pass element as an argiment to the parseInt function like that
// let newArray = ["1", "2", "3"].map(number => parseInt(number))
// console.log(newArray);
// another ways 
// console.log(['1', '2', '3'].map(Number) );

